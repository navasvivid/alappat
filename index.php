<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<?php include("gtag.php"); ?>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="SemiColonWeb" />

<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="style.css?v=1.2" type="text/css" />
<link rel="stylesheet" href="css/dark.css" type="text/css" />

<link rel="stylesheet" href="demos/real-estate/real-estate.css?v=<?=time()?>" type="text/css" />
<link rel="stylesheet" href="demos/real-estate/css/font-icons.css" type="text/css" />

<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
<link rel="stylesheet" href="css/animate.css" type="text/css" />
<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

<link rel="stylesheet" href="demos/real-estate/fonts.css" type="text/css" />



<link rel="stylesheet" href="css/responsive.css" type="text/css" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!--[if lt IE 9]>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->

<meta property="og:url"           content="http://www.houseofalapattproperties.com/" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="Alapatt Properties" />
<meta property="og:description"   content="Blessed with the awe-inspiring vista of the Marari beach, the 10 acre spanning property offers an ideal location for the construction of private residences, resorts or tourist-friendly establishments" />
<meta property="og:image"         content="http://www.houseofalapattproperties.com/images/gallery/pic14.jpg" />

<link rel="stylesheet" href="css/colors.php?color=2C3E50" type="text/css" />

<title>House of alapatt properties</title>
<style> 
@media(max-width: 768px){
.mob-height{height: auto!important;
	}
	.gmap iframe{
		height: 100vh!important;
	}
}
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin: 0; 
}
</style>
 <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KL2J58Z');</script>
        <!-- End Google Tag Manager -->
</head>

<body class="stretched side-push-panel">

	 <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KL2J58Z"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

<div id="wrapper" class="clearfix">


<div id="top-bar" class="transparent-topbar hidden-sm hidden-xs">

<div class="container clearfix">

<div class="col_half nobottommargin clearfix">


<div class="top-links">
<ul>
<li><a href="#">Cochin</a></li>
</ul>
</div>

</div>

<div class="col_half fright col_last clearfix nobottommargin">


<div class="top-links">
<ul>
<li class="hidden-sm"><a href="#"><i class="icon-call"></i>+91 98469 11122</a></li>

<li> 
<a href="http://www.facebook.com/sharer/sharer.php?u=http://www.houseofalapattproperties.com/t=Beachfront Property at Mararikulam" target="_blank" class="share-popup">Share on facebook</a>
</li>
</ul>
</div>

</div>

</div>

</div>
<header id="header" class="static-sticky transparent-header dark">

<div id="header-wrap">

<div class="container clearfix">

<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>


<div id="logo">
<a href="#" data-dark-logo="demos/real-estate/images/logo.png" class="standard-logo"><img src="demos/real-estate/images/logo.png" alt="Canvas Logo"></a>
<a href="#" data-dark-logo="demos/real-estate/images/logo@2x.png" class="retina-logo"><img src="demos/real-estate/images/logo@2x.png" alt="Canvas Logo"></a>
</div>
<nav id="primary-menu" class="with-arrows">

<ul class="one-page-menu">

<li class="current"><a data-href="#header" href="#"><div>Home</div></a></li>
<li><a data-href="#about-us" href="#"><div>About Us</div></a></li>
<li><a href="https://www.houseofalapattproperties.com/dlf-riverside-luxury-flat-kochi/"><div>4BHK Flat</div></a></li>
<li><a data-href="#gallery" href="#"><div>Gallery</div></a></li>

<li><a data-href="#contact" href="#"><div>Contact</div></a></li>
</ul>

</nav>

</div>

</div>

</header>
<section id="slider" class="force-full-screen full-screen dark clearfix">

<div class="force-full-screen full-screen">
<div class="fslider" data-speed="3000" data-pause="7500" data-animation="fade" data-arrows="false" data-pagi="false" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; background-color: #333; z-index: 1;">
<div class="flexslider" style="height: 100% !important;">
<div class="slider-wrap" style="height: inherit;">
<div class="slide" style="background: url('images/gallery/pic14.jpg') center center; background-size: cover; height: 100% !important;"></div>



</div>
</div>
</div>

<div class="real-estate-tabs-slider index" style="z-index:99">
<div class="container text-center clearfix">
	<span class="uppercase hidden-sm hidden-md hidden-lg" style="font-size: 18px; letter-spacing: 1px; color: rgba(255,255,255,0.9);">A Few Steps Away For Your Dream Property.</span>
<div class="tabs advanced-real-estate-tabs nomargin clearfix ui-tabs ui-corner-all ui-widget ui-widget-content" style="max-width: 320px;">

<div class="tab-container">

<div class="tab-content clearfix ui-tabs-panel ui-corner-bottom ui-widget-content" id="tab-rent" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true">
<div class="quick-contact-widget dark clearfix">

<div class="quick-contact-form-result"></div>

<form id="quick-contact-form" name="quick-contact-form" action="include/quickcontact.php?from=properties" method="post" class="quick-contact-form nobottommargin">

<div class="form-process"></div>

<input type="text" class="required sm-form-control input-block-level not-dark" id="quick-contact-form-name" name="quick-contact-form-name" value="" placeholder="Name" />

<input type="email" class="required sm-form-control email input-block-level not-dark" id="quick-contact-form-email" name="quick-contact-form-email" value="" placeholder="Email" />

<input type="number" class="required sm-form-control input-block-level not-dark" id="quick-contact-form-phone" name="quick-contact-form-phone" value="" placeholder="Mobile" />

<textarea class="sm-form-control input-block-level not-dark short-textarea" id="quick-contact-form-message" name="quick-contact-form-message" rows="5" cols="30" placeholder="Message"></textarea>

<input type="text" class="hidden" id="quick-contact-form-botcheck" name="quick-contact-form-botcheck" value="" />

<button type="submit" id="quick-contact-form-submit" name="quick-contact-form-submit" class="button button-3d button-rounded btn-block nomargin" value="submit">Send Email</button>

</form>

</div>
</div>
</div>
</div>
</div>
</div>

<div class="vertical-middle" style="z-index: 3;">
<div class="container center clearfix">
<div class="emphasis-title nomargin hidden-xs text-right">
<h3 style="font-size: 36px;">Your Dream Property.</h3>
<span class="uppercase" style="font-size: 18px; letter-spacing: 1px; color: rgba(255,255,255,0.9);">A Few Steps Away.</span>
</div>
</div>
</div>
<div class="video-wrap" style="position: absolute; top: 0; left: 0; height: 100%; z-index:1;">
<div class="video-overlay real-estate-video-overlay" style="z-index: 1;"></div>
</div>
</div>

</section>


<section id="content">

<div class="content-wrap">

<div class="container clearfix">


<div class="about-us" id="about-us">

<div class="heading-block center">
<h2>About Us</h2>
<span></span>
</div>

<div class="col_one_third nobottommargin" style="margin-top: -30px;">
	<img style="max-width: 250px;" class="img-responsive" src="images/Alapatt_Building Kochi_transperant_BG.png" alt="alapatt"/>
</div>
<div class="col_two_third topmargin-sm nobottommargin col_last">

<p>
Since inception, House of Alappatt has moved forward with every successful venture for the past 65 years. Synonymous with exceptional customer relationship and transparency in business, the enterprise began with the defined vision and unwavering ideals of the late P.T. Antony. Thanks to the forward-thinking market strategies of his son, Mr. Jose Alapatt, the name of Alappatt has been in the forefront of the jewellery business for decades. Under his visionary guidance, House of Alapatt continues to grow from strength to strength.</p>

</div>
</div>

<div class="clear"></div>
<div class="line topmargin-sm bottommargin-sm"></div>

<div class="about-property" id="about-property">

<div class="heading-block center">
<h2>The Property</h2>
<span>Beachfront Property at Mararikulam</span>
</div>


<div class="col_full">
<p>
Blessed with the awe-inspiring vista of the Marari beach, the 10 acre spanning property offers an ideal location for the construction of private residences, resorts or tourist-friendly establishments. Currently under the care of the Real Estate Division of House of Alapatt, M G Road, Ernakulam, the property has locational advantages including 100m road frontage and 150m beach frontage. The present market price of the plot is 5 lakh rupees per cent. </p>
</div>


</div>

<div class="clear"></div>
<div class="line topmargin-sm bottommargin-sm"></div>


<div class="gallery" id="gallery">

<div class="heading-block center">
<h2>Gallery</h2>
<span>Alapatt Property “How Real Estate Gets Real”</span>
</div>



<div data-lightbox="gallery" class="row real-estate-properties clearfix">

<div class="col-md-4">
<a data-lightbox="gallery-item" href="images/gallery/pic15.jpg"  style="background: url('images/gallery/pic15.jpg') no-repeat bottom center; background-size: cover;">

</a>
</div>
<div class="col-md-4">
<a data-lightbox="gallery-item" href="images/gallery/h1.jpg" style="background: url('images/gallery/h1.jpg') no-repeat center center; background-size: cover;">

</a>
</div>

<div class="col-md-4">
<a href="images/gallery/pic22.jpg" data-lightbox="gallery-item" style="background: url('images/gallery/pic22.jpg') no-repeat center center; background-size: cover;">
</a>
</div>

<div class="clear"></div>

<div class="col-md-4">
<a href="images/gallery/pic28.jpg" data-lightbox="gallery-item" style="background: url('images/gallery/pic28.jpg') no-repeat center center; background-size: cover;">

</a>
</div>
<!-- <div class="col-md-4">
<a href="images/gallery/pic33.jpg" data-lightbox="gallery-item" style="background: url('images/gallery/pic33.jpg') no-repeat center center; background-size: cover;">

</a>
</div> -->




<!-- <div class="col-md-4">
<a href="images/gallery/pic35.jpg" data-lightbox="gallery-item" style="background: url('images/gallery/pic35.jpg') no-repeat center center; background-size: cover;">

</a>
</div> -->


<div class="col-md-4">
<a href="images/gallery/pic40.jpg" data-lightbox="gallery-item" style="background: url('images/gallery/pic40.jpg') no-repeat center center; background-size: cover;">

</a>
</div>


<!-- <div class="col-md-4">
<a href="images/gallery/new/1.jpg" data-lightbox="gallery-item" style="background: url('images/gallery/new/1.jpg') no-repeat center center; background-size: cover;">

</a>
</div> -->
<div class="col-md-4">
<a href="images/gallery/new/2.jpg" data-lightbox="gallery-item" style="background: url('images/gallery/new/2.jpg') no-repeat center center; background-size: cover;">

</a>
</div>
<!-- <div class="col-md-4">
<a href="images/gallery/new/3.jpg" data-lightbox="gallery-item" style="background: url('images/gallery/new/3.jpg') no-repeat center center; background-size: cover;">

</a>
</div> -->
<!-- <div class="col-md-4">
<a href="images/gallery/new/4.jpg" data-lightbox="gallery-item" style="background: url('images/gallery/new/4.jpg') no-repeat center center; background-size: cover;">

</a>
</div> -->
<!-- <div class="col-md-4">
<a href="images/gallery/new/5.jpg" data-lightbox="gallery-item" style="background: url('images/gallery/new/5.jpg') no-repeat center center; background-size: cover;">

</a>
</div> -->
<!-- <div class="col-md-4">
<a href="images/gallery/new/6.jpg" data-lightbox="gallery-item" style="background: url('images/gallery/new/6.jpg') no-repeat center center; background-size: cover;">

</a>
</div> -->
<!-- <div class="col-md-4">
<a href="images/gallery/new/7.jpg" data-lightbox="gallery-item" style="background: url('images/gallery/new/7.jpg') no-repeat center center; background-size: cover;">

</a>
</div> -->
<div class="col-md-4">
<a href="images/gallery/new/8.jpg" data-lightbox="gallery-item" style="background: url('images/gallery/new/8.jpg') no-repeat center center; background-size: cover;">

</a>
</div>

<div class="col-md-4">
<a href="images/gallery/new/9.jpg" data-lightbox="gallery-item" style="background: url('images/gallery/new/9.jpg') no-repeat center center; background-size: cover;">

</a>
</div>

<!-- <div class="col-md-4 ">
<div class="entry-image">
		
		<iframe data-lightbox="iframe" width="560" height="380" src="https://www.youtube.com/embed/61gCvDh5XbQ" frameborder="0" allowfullscreen></iframe>


</div>
</div> -->

<div class="col-md-4 ">
<div class="entry-image">
		
<iframe data-lightbox="iframe" width="560" height="380" src="https://www.youtube.com/embed/AtQ-5SOoOsM?rel=0" frameborder="0" allowfullscreen></iframe>


</div>
</div>


</div>

</div>
</div>

<div class="col-md-3 mob-height visible-xs" style="background-color: #E5E5E5;">
<div style="padding: 40px;">
<h4 class="font-body t600 ls1">Contact Us</h4>

<div style="font-size: 15px; line-height: 1.7;">
<address style="line-height: 1.7;">
<strong>Kerala:</strong><br>
House of Alapatt,<br>
MG Road, Cochin - 682016<br><br>
<span title="Phone Number"><strong>Latitude: </strong></span>9.57922<br>
<span title="Email Address"><strong>Longitude: </strong></span>76.302559

<br/><br/>
<span title="Phone Number"><strong>Phone:</strong></span>+91 98469 11122<br>
<span title="Email Address"><strong>Email:</strong></span> greenpeace@gmail.com
</address>



</div>
</div>
</div>
<p class="visible-xs nomargin" style="height:3px;">&nbsp;</p>

<div id="contact" class="row contact norightmargin  bottommargin-lg common-height">
<div class="col-md-5 gmap"> 
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7465.311670541613!2d76.29839982093799!3d9.579222198448424!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zOcKwMzQnNDUuMiJOIDc2wrAxOCcwOS4yIkU!5e0!3m2!1sen!2sin!4v1510224753206" width="100%" height="100vh" frameborder="0" style="border:0;width: 100%;" allowfullscreen></iframe>

	<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d7868.3722245970075!2d76.302559!3d9.57922!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1516427445709" width="100%" height="100vh" frameborder="0" style="border:0;width: 100%;" allowfullscree></iframe> -->
<!-- 
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3934.186051082422!2d76.30037031417471!3d9.579225282767231!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zOcKwMzQnNDUuMiJOIDc2wrAxOCcwOS4yIkU!5e0!3m2!1sen!2sin!4v1516427774624" width="100%" height="100vh" frameborder="0" style="border:0;width: 100%;" allowfullscree></iframe> -->
</div>

<div class="col-md-3 mob-height hidden-xs" style="background-color: #E5E5E5;">
<div style="padding: 40px;">
<h4 class="font-body t600 ls1">Contact Us</h4>

<div style="font-size: 15px; line-height: 1.7;">
<address style="line-height: 1.7;">
<strong>Kerala:</strong><br>
House of Alapatt,<br>
MG Road, Cochin - 682016<br><br>
<span title="Phone Number"><strong>Latitude: </strong></span>9.57922<br>
<span title="Email Address"><strong>Longitude: </strong></span>76.302559

<br/><br/>
<span title="Phone Number"><strong>Phone:</strong></span>+91 98469 11122<br>
<span title="Email Address"><strong>Email:</strong></span> greenpeace@gmail.com
</address>

<div class="clear topmargin-sm"></div>

</div>
</div>
</div>

<div class="col-md-4 bgcolor">
<div class="col-padding">
<div class="quick-contact-widget dark clearfix">

<h3 class="capitalize ls1 t400">Get in Touch</h3>

<div class="quick-contact-form-result"></div>

<form id="quick-contact-form" name="quick-contact-form" action="include/quickcontact.php?from=properties" method="post" class="quick-contact-form nobottommargin">

<div class="form-process"></div>

<input type="text" class="required sm-form-control input-block-level not-dark" id="quick-contact-form-name" name="quick-contact-form-name" value="" placeholder="Name" />

<input type="email" class="required sm-form-control email input-block-level not-dark" id="quick-contact-form-email" name="quick-contact-form-email" value="" placeholder="Email" />

<input type="number" class="required sm-form-control input-block-level not-dark" id="quick-contact-form-phone" name="quick-contact-form-phone" value="" placeholder="Mobile" />

<textarea class="sm-form-control input-block-level not-dark short-textarea" id="quick-contact-form-message" name="quick-contact-form-message" rows="5" cols="30" placeholder="Message"></textarea>

<input type="text" class="hidden" id="quick-contact-form-botcheck" name="quick-contact-form-botcheck" value="" />

<button type="submit" id="quick-contact-form-submit" name="quick-contact-form-submit" class="button button-small button-rounded button-light button-white nomargin" value="submit">Send Email</button>

</form>

</div>
</div>
</div>
</div>

<div class="container clear-bottommargin clearfix">

<div class="promo promo-dark promo-flat bottommargin-lg one-page-menu">
<h3 class="t400 ls1">Special Offers on Villa Long Term Rentals &amp; Lease Agreements</h3>
<a  data-href="#contact" class="button button-dark button-large button-rounded">Contact Now</a>
</div>


</div>

</div>

</section>
<footer id="footer" class="dark">

<div class="container">


<div class="footer-widgets-wrap nomargin nopadding center clearfix">

<div class="col_full">

<div class="widget clearfix">

<img src="demos/real-estate/images/logo@2x.png" class="footer-logo"  alt="Footer Logo">




<p class="ls1 t300" style="opacity: 0.6; font-size: 13px;">Copyrights &copy; 2017 Alapatt: Real Estate</p>

</div>

</div>



</div>

</div>

</footer>

</div>


<div id="gotoTop" class="icon-angle-up"></div>


<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/plugins.js"></script>
<?php include("chat-script.php"); ?>
<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>